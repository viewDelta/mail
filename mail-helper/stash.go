package mailhelper

import (
	"bitbucket.org/viewDelta/service-core/models"
	mailerrorhistory "bitbucket.org/viewDelta/service-core/repository/mail-error-history"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	mailErrorDAO = mailerrorhistory.New()
)

//MailErrorStatus will make db entry for failed/success mail if they have entry
func MailErrorStatus(isSuccess bool, email, xtype string) error {
	log.Debug("Entering mailhelper.MailErrorStatus function")
	if emailError, err := mailErrorDAO.FindAnyExistingError(email, xtype); err != nil {
		if err == mongo.ErrNoDocuments && !isSuccess {
			//Creating instance of mail_error_history
			if err := mailErrorDAO.InsertMailError(models.NewMailErrorHistory(email, xtype)); err != nil {
				log.Errorf("Mail_Error_Histor Insertion Error - [%v]", err)
				return err
			}
		} else {
			log.Errorf("Record Fetch Error - [%v]", err)
			return err
		}
	} else {
		emailError.Attempts = emailError.Attempts + 1
		if isSuccess {
			emailError.IsSuccess = true
		}
		//Creating instance of mail_error_history
		if err := mailErrorDAO.UpdateMailError(emailError); err != nil {
			log.Errorf("Mail_Error_Histor Updation Error - [%v]", err)
			return err
		}
	}
	log.Debug("Exiting mailhelper.MailErrorStatus function")
	return nil
}
