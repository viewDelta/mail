package mailhelper

import (
	"bytes"
	"fmt"
	"html/template"
	"os"
	"strconv"

	"bitbucket.org/viewDelta/mail/boot"
	"bitbucket.org/viewDelta/service-core/constants"
	"github.com/go-gomail/gomail"
	"github.com/labstack/gommon/log"
)

const (
	//SignUp type of mail to be sent i.e provided to SUBJECT of MAIL struct object
	SignUp string = "WELCOME INVITE"
	//ForgetPassword type of mail to be sent i.e provided to SUBJECT of MAIL struct object
	ForgetPassword string = "RESET PASSWORD"

	forgotPwdMailTemplateLocation string = "/assets/forgotPwd.html"
	signupMailTemplateLocation    string = "/assets/signUp.html"
)

//MAIL struct to trigger mail request
type MAIL struct {
	TO, NAME, SUBJECT, WebURL, VerifyEmailURL, TOKEN string
}

//SendMail will trigger mail based on mail ID
func SendMail(mail *MAIL) error {
	var (
		tpl bytes.Buffer
		dir string
		err error
	)

	//File Location Validation
	if dir, err = os.Getwd(); err != nil {
		log.Errorf("File location couldn't fetched [%v]", err)
		return fmt.Errorf(constants.TEMPLATE_FILE_NOT_FOUND)
	}

	//Setting Up Mail Client
	m := gomail.NewMessage()
	m.SetHeader("From", boot.FROM)
	m.SetHeader("To", mail.TO)

	switch mail.SUBJECT {
	case SignUp:
		t, err := template.ParseFiles(dir + signupMailTemplateLocation)
		if err != nil {
			log.Errorf("Template Parsing Error [%v]", err)
			return fmt.Errorf(constants.TEMPLATE_PARSING_ERROR)
		}
		data := &struct {
			NAME, WebURL, APIURL, TOKEN string
		}{
			NAME:   mail.NAME,
			WebURL: mail.WebURL,
			APIURL: mail.VerifyEmailURL,
			TOKEN:  mail.TOKEN,
		}
		if err := t.Execute(&tpl, data); err != nil {
			log.Errorf("Template Execution Error [%v]", err)
			return fmt.Errorf(constants.TEMPLATE_PARSING_ERROR)
		}
		m.SetHeader("Subject", SignUp)
		m.SetBody("text/html", tpl.String())

	case ForgetPassword:
		t, err := template.ParseFiles(dir + forgotPwdMailTemplateLocation)
		if err != nil {
			log.Errorf("Template Parsing Error [%v]", err)
			return fmt.Errorf(constants.TEMPLATE_PARSING_ERROR)
		}
		data := &struct {
			NAME, WebURL, TOKEN string
		}{
			NAME:   mail.NAME,
			WebURL: mail.WebURL,
			TOKEN:  mail.TOKEN,
		}
		if err := t.Execute(&tpl, data); err != nil {
			log.Errorf("Template Execution Error [%v]", err)
			return fmt.Errorf(constants.TEMPLATE_PARSING_ERROR)
		}
		m.SetHeader("Subject", ForgetPassword)
		m.SetBody("text/html", tpl.String())
	}

	port, err := strconv.Atoi(boot.PORT)
	if err != nil {
		return fmt.Errorf(constants.PORT_VALUE_EXPECTED)
	}
	d := gomail.NewDialer(boot.SMTP, port, boot.FROM, boot.PASSWORD)
	if err := d.DialAndSend(m); err != nil {
		log.Errorf("Error While Sending Mail [%v]", err)
		return fmt.Errorf(constants.UNABLE_TO_SEND_MAIL)
	}
	return nil
}
