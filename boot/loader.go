package boot

import (
	"bitbucket.org/viewDelta/service-core/config"
)

//Mail Configuration Properties
var (
	FROM     = config.GetEnv("FROM")
	PASSWORD = config.GetEnv("PASSWORD")
	SMTP     = config.GetEnv("SMTP")
	PORT     = config.GetEnv("PORT")
)
