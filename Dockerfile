#Simple Docker Image
FROM alpine:3.11
LABEL Author="Abinash <apattajoshi@outlook.com>" 
# Expose port 8082 to the outside world
EXPOSE 8888 

#Work Location Image
ARG APP_HOME="/usr/local/mailX/" 
ARG LOG_DIR="/var/retailGo/logs/"
ARG BinaryDir="bin/" 

RUN mkdir -p ${APP_HOME}
RUN mkdir -p ${LOG_DIR}
RUN chmod 777 -R ${LOG_DIR}
#Log File Directory Exposed which will be mapped during containerization
VOLUME [ ${LOG_DIR} ]

COPY ${BinaryDir} ${APP_HOME} 
WORKDIR ${APP_HOME}
CMD ["./mail"]
