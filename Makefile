.PHONY: clean
clean:
	rm -rf bin

.PHONY: build
build: clean
	# env GOOS=linux GOARCH=amd64 go build -o bin/mail
	#Alpine:3.11
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o bin/mail
	chmod +x bin/mail
	cp *.yml bin/
	cp -r assets bin/
