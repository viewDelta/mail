package handlers

import (
	"net/http"

	"bitbucket.org/viewDelta/service-core/config"
	"bitbucket.org/viewDelta/service-core/locales/request"

	"github.com/dennisstritzke/httpheader"

	mailhelper "bitbucket.org/viewDelta/mail/mail-helper"
	apphelper "bitbucket.org/viewDelta/service-core/app-helper"
	"bitbucket.org/viewDelta/service-core/constants"
	endpoints "bitbucket.org/viewDelta/service-core/constants"
	settings "bitbucket.org/viewDelta/service-core/constants"
	helper "bitbucket.org/viewDelta/service-core/app-helper"
	"bitbucket.org/viewDelta/service-core/constants/proxy"
	"bitbucket.org/viewDelta/service-core/headers"
	"bitbucket.org/viewDelta/service-core/repository/users"
	"bitbucket.org/viewDelta/service-core/utils"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
)

var (
	userDao    = users.New()
	ivalidator utils.Ivalidator
)

func init() {
	if validator, err := utils.NewValidatorInstance(false); err != nil {
		log.Errorf("Unable to Initiate Core-Validator instance")
	} else {
		ivalidator = validator
	}
}

// SendMail will send the signup mail for the defined in request body
// @Summary Sends SignUp or Reset password mail
// @Description SendMail will send the signup mail for the defined in request body
// @Tags mail
// @Accept json
// @Produce json
// @Param Client header string true "RetailGO"
// @Param X-Type header string true "SignUp/Reset"
// @Param mailId body locales.MailReq true "activate user body"
// @Success 200 {object} locales.ResponseTemplate "mail sent successfully"
// @Failure 400 {object} locales.ResponseTemplate "Either header is Missing or Invalid or invalid Email in body"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /sendMail [post]
func SendMail(c echo.Context) error {
	var (
		mail       mailhelper.MAIL
		reqHeaders headers.EmailHeader
		reqBody    request.MailReq
	)
	//Header validation --> Client, X-Type [SignUp/Reset]
	if err := httpheader.Bind(c.Request().Header, &reqHeaders); err != nil {
		log.Errorf("Header parsing error [%v]", err)
		return apphelper.SendGenericResponse(c, http.StatusBadRequest, constants.INVALID_HEADER)
	}
	if err := reqHeaders.ValidateHeader(); err != nil {
		log.Errorf("Invalid header requested [%v]", err)
		return apphelper.SendGenericResponse(c, http.StatusBadRequest, err.Error())
	}
	//Request Body --> EMAIL
	if err := c.Bind(&reqBody); err != nil {
		return helper.SendGenericResponse(c, http.StatusBadRequest, constants.INVALID_PAYLOAD)
	}
	if _, ok := ivalidator.ValidateEmailExsits(reqBody.Email); !ok {
		return apphelper.SendGenericResponse(c, http.StatusBadRequest, constants.INVALID_EMAIL)
	}
	log.Infof("Requested Trace ==> {%v} {ReqBy - %v}", reqHeaders.String(), reqBody.Email)
	//User to update token
	user, _ := userDao.FindUser(reqBody.Email)
	tokenDetails, err := utils.GetToken(user, reqHeaders.Client)
	if err != nil {
		log.Errorf("Token Creation Error [%v]", err)
		return apphelper.SendGenericResponse(c, http.StatusInternalServerError, constants.TOKEN_GENERATION_ERROR)
	}
	//Mail Struct init
	mail.TO = reqBody.Email
	mail.NAME = user.FirstName
	mail.TOKEN = tokenDetails.TokenID

	if reqHeaders.IsSignUpReq() {
		//When Already request id has been isssued
		if user.VerifyRequest.Id != "" {
			return apphelper.SendGenericResponse(c, http.StatusBadRequest, constants.SIGNUP_MAIL_ALREADY_REQUESTED)
		}
		mail.SUBJECT = mailhelper.SignUp
		mail.WebURL = config.GetConfiguration().Network.Protocol + c.Request().Header.Get(settings.X_Forwarded_Host)
		mail.VerifyEmailURL = config.GetConfiguration().Network.Protocol + c.Request().Header.Get(settings.X_Forwarded_Host) + proxy.VERIFY_ACCOUNT_ENDPOINT
		user.VerifyRequest.Id = tokenDetails.TokenID
		user.VerifyRequest.ReqAt = tokenDetails.ReqAt
		user.VerifyRequest.ExpAt = tokenDetails.ExpAt
	} else {
		if user.Active != true || user.Verified != true {
			return apphelper.SendGenericResponse(c, http.StatusBadRequest, constants.USER_ACCOUNT_NOT_ACTIVE_OR_VERIFIED)
		}
		//Forgot password check if token has been issued
		mail.SUBJECT = mailhelper.ForgetPassword
		mail.WebURL = config.GetConfiguration().Network.Protocol + c.Request().Header.Get(settings.X_Forwarded_Host) + endpoints.ResetPage //WebURL for resetting mail
		user.ResetRequest.Id = tokenDetails.TokenID
		user.ResetRequest.ReqAt = tokenDetails.ReqAt
		user.ResetRequest.ExpAt = tokenDetails.ExpAt
	}
	if err := userDao.UpdateUser(user); err != nil {
		return apphelper.SendGenericResponse(c, http.StatusInternalServerError, constants.RECORD_UPDATATION_ERROR)
	}
	if err := mailhelper.SendMail(&mail); err != nil {
		log.Errorf("Unable to send the mail - [%v]", err)
		mailhelper.MailErrorStatus(false, mail.TO, reqHeaders.XType)
		return apphelper.SendGenericResponse(c, http.StatusInternalServerError, err.Error())
	}
	mailhelper.MailErrorStatus(true, mail.TO, reqHeaders.XType)
	return apphelper.SendGenericResponse(c, http.StatusOK, constants.MAIL_SENT_SUCCESS)
}
