module bitbucket.org/viewDelta/mail

go 1.14

require (
	bitbucket.org/viewDelta/service-core v1.1.0
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/dennisstritzke/httpheader v0.0.0-20191001125049-c4997e500c10
	github.com/go-gomail/gomail v0.0.0-20160411212932-81ebce5c23df
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.1.16
	github.com/labstack/gommon v0.3.0
	github.com/robfig/cron/v3 v3.0.1
	github.com/swaggo/echo-swagger v1.0.0
	github.com/swaggo/swag v1.6.3
	go.mongodb.org/mongo-driver v1.3.4
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df // indirect
)