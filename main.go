package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"time"

	_ "bitbucket.org/viewDelta/mail/docs"
	"bitbucket.org/viewDelta/mail/handlers"
	mailcron "bitbucket.org/viewDelta/mail/mail-cron"
	"bitbucket.org/viewDelta/service-core/config"
	endpoints "bitbucket.org/viewDelta/service-core/constants"
	settings "bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/headers"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	echoSwagger "github.com/swaggo/echo-swagger"
)

// @title RetailMail
// @description This is mailing service for application.
// @version 1.0
// @contact.name Abinash Pattajoshi
// @host localhost:8888
// @BasePath /mail
// @tag.name mail
// @tag.description Mailing service module
// @schemes http

func main() {
	//Read Property Loader
	prop := config.GetConfiguration()

	//Echo Router
	router := echo.New()
	router.Use(
		middleware.Secure(),
		middleware.Recover(),
		middleware.RequestID(),
	)
	router.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{config.GetEnv("ORIGIN")},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, headers.XTYPE, headers.CLIENT, settings.X_Forwarded_Host},
		AllowMethods: []string{http.MethodHead, http.MethodPost, http.MethodOptions},
	}))
	router.HTTPErrorHandler = func(err error, c echo.Context) {
		// Take required information from error and context and send it to a service like New Relic
		log.Error(c.Path(), c.QueryParams(), err.Error())
		// Call the default handler to return the HTTP response
		router.DefaultHTTPErrorHandler(err, c)
	}
	router.Debug = true
	router.Logger.SetLevel(log.DEBUG)
	group := router.Group("/mail")
	{
		group.POST(endpoints.SendMail, handlers.SendMail)
	}
	router.GET("/swagger/*", echoSwagger.WrapHandler)
	log.Infof("%v-%v Started on --> %v", prop.Application.Name, prop.Application.Version, prop.Application.Port)
	// Start server
	go func() {
		if err := router.Start(":" + prop.Application.Port); err != nil {
			log.Infof("shutting down the server %v", err)
		}
	}()
	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	defer func() {
		mailcron.StopCronScheduler()
		//Closing the log file upon receiving the signal
		config.CloseLogFile()
	}()
	if err := router.Shutdown(ctx); err != nil {
		router.Logger.Fatal(err)
	}
}
