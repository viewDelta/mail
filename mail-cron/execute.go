package mailcron

import (
	mailhelper "bitbucket.org/viewDelta/mail/mail-helper"
	"bitbucket.org/viewDelta/service-core/constants/enums"
	"bitbucket.org/viewDelta/service-core/headers"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/outbound"
	mailerrorhistory "bitbucket.org/viewDelta/service-core/repository/mail-error-history"
	"github.com/labstack/gommon/log"
	"github.com/robfig/cron/v3"

	"bitbucket.org/viewDelta/service-core/config"
	"bitbucket.org/viewDelta/service-core/repository"
	"bitbucket.org/viewDelta/service-core/utils"
)

var (
	cronEnable          bool
	cronExp             string
	maxAttempts         int
	cronJobQualifier    []string
	mailCron            *cron.Cron
	mailRetryFunction   func()
	mailErrorHistoryDAO repository.MailErrorHistoryDAO
)

func init() {
	log.Info("Checking CronEnable status for cronJob")
	cronEnable = config.GetConfiguration().RetryJob.Enable
	if cronEnable {
		log.Info("Found Mail Cron Status [Enabled]")
		if cronExp = config.GetConfiguration().RetryJob.CronExp; utils.IsBlankString(cronExp) {
			log.Panicf("Expected Value of the Field - cronExp")
		}
		if cronJobQualifier = config.GetConfiguration().RetryJob.Qualifier; utils.IsBlankSlice(cronJobQualifier) {
			log.Panicf("Expected Value of the Field - cronJobQualifier")
		}
		//validating X-Type Match
		validateQualifier(cronJobQualifier)
		maxAttempts = config.GetConfiguration().RetryJob.MaxAttempts
		mailErrorHistoryDAO = mailerrorhistory.New()
		initJob()
	} else {
		log.Info("Found Mail Cron Status [Disabled]")
	}
}

func initJob() {
	var (
		allFailedMailList []models.MailErrorHistory
		err               error
		signUpheaders     = headers.GetSignUpHeader()
		resetheaders      = headers.GetResetHeader()
		host              = config.GetEnv("ORIGIN")
	)

	mailRetryFunction = func() {
		log.Debug("Entering mailcron.cronJob function")
		log.Infof("Fetching All Failed Email Request based on Qualifer - %v", cronJobQualifier)
		if len(cronJobQualifier) == 1 {
			allFailedMailList, err = mailErrorHistoryDAO.FindAllOnlyOnXType(cronJobQualifier[0], maxAttempts)
		} else {
			allFailedMailList, err = mailErrorHistoryDAO.FindAllNotSent(maxAttempts)
		}
		if err != nil {
			log.Errorf("Record Fetch Error - %v", err)
			return
		}
		for _, v := range allFailedMailList {
			if v.XType == enums.SIGNUP {
				err = outbound.MailCall(signUpheaders, v.Email, host)
			} else {
				err = outbound.MailCall(resetheaders, v.Email, host)
			}
			if err != nil {
				log.Errorf("OutBound Mail Call failed. Updating in failed mail List")
				mailhelper.MailErrorStatus(false, v.Email, v.XType)
			} else {
				log.Infof("OutBound Mail Call success. Updating in failed mail List")
				mailhelper.MailErrorStatus(true, v.Email, v.XType)
			}
		}
		log.Debug("Exiting mailcron.cronJob function")
	}
	mailCron = cron.New()
	id, err := mailCron.AddFunc(cronExp, mailRetryFunction)
	if err != nil {
		log.Errorf("Cron Job Error - [%v]", err)
		return
	}
	log.Infof("Cron job Added With ID - %v", id)
	mailCron.Start()
}

//StopCronScheduler to stop mail cronJobs
func StopCronScheduler() {
	if mailCron != nil {
		log.Info("Closing Scheduled Jobs Now")
		mailCron.Stop()
	}
}

func validateQualifier(input []string) {
	allXTypes := headers.GetAllXTypes()
	for _, v := range input {
		if !utils.Contains(allXTypes, v) {
			log.Panicf("Invalid Qualifier/X-Type provided")
		}
	}
}
